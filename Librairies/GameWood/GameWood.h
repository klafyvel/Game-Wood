#ifndef H_GAMEWOOD
#define H_GAMEWOOD

#include "TVout.h"

#define BT_UP           2
#define BT_DOWN         12
#define BT_LEFT         4
#define BT_RIGHT        3
#define BT_A            6
#define BT_B            8

void initGameWood();

#endif
